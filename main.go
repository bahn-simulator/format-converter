package main

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"strconv"
	"strings"
)

//This struct defines the data structure of the json format.
//Each frame has an allocated location along the track.
//The unit of the location is meter
type point struct {
	Frame    int64   `json:"frame"`
	Location float64 `json:"location"`
}

func main() {
	filename := "track.csv"
	hasDescription := false
	startingIndex := 0
	comma := " "

	//check the command line arguments
	argsWithoutProg := os.Args[1:]
	if len(argsWithoutProg) > 0 {
		filename = argsWithoutProg[0]
	}

	if len(argsWithoutProg) > 1 {
		comma = argsWithoutProg[1]
	}

	if len(argsWithoutProg) > 2 {
		desc := argsWithoutProg[2]
		if desc == "true" {
			hasDescription = true
			startingIndex = 1
		}
	}

	//read the csv file into an array and prepare json aray
	dat, err := readCSVFile(filename, comma)

	if err != nil {
		log.Fatalln(err.Error())
	}

	jsonDat := make([]point, len(dat))

	//pase the csv to the right format for json export
	for i := startingIndex; i < len(dat); i++ {
		j := i
		if hasDescription {
			j -= 1
		}

		//correct the comma format from , to .
		dat[i][1] = strings.ReplaceAll(dat[i][1], ",", ".")

		//parse the frame
		frame, err := strconv.ParseInt(dat[i][0], 10, 64)
		if err != nil {
			jsonDat[j].Frame = 0
		}
		jsonDat[j].Frame = frame

		//parse the location
		location, err := parseFloat(dat[i][1])
		if err != nil {
			jsonDat[j].Location = 0
		}
		jsonDat[j].Location = location
	}

	jOutput, err := json.MarshalIndent(jsonDat, "", "")
	if err != nil {
		log.Fatalln(err.Error())
	}

	err = ioutil.WriteFile(strings.ReplaceAll(filename, filepath.Ext(filename), ".json"), jOutput, 0644)

	if err != nil {
		log.Fatalln(err.Error())
	}

	log.Println("Converting to json was successful.")

}
