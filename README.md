# Format Converter

This tool converts the track from the old csv format to the new json format.

## Usage

First you have to compile the project.
In order to this install [go](https://golang.org/).
After that clone the repository and open a terminal in that folder.
Now type `go run . filename` to convert a file with the `filename` to a json file in the same folder.

## Formats

### CSV (input)

The csv file needs 2 columns seperated by a semicolon.
The first column contains all the frames of the video file while the second column contains the location along the track in meters.
The number for the location can either have a comma or a dot a decimal place.
The first row will be ignored by the parser but it also needs two columns.

Example:

```csv
Frame Number; location on track
0 0
1 0
2 0
3 0,02132163
4 0,02132163
5 0,02132163
6 0,02132163
7 0,02132163
8 0,04264326
9 0,04264326
```

### JSON (output)

The json file will be a root array of points.
Each point has two properties, the frame and location.
The frame is an integer specifying where in the video this point is.
The location specifies the location of this point along the track.
The location is a value in meters and it is a decimal number with a dot as decimal place.

Example:

```json
[
    {
        "frame": 0,
        "location": 0
    },
    {
        "frame": 1,
        "location": 0
    },
    {
        "frame": 2,
        "location": 0
    },
    {
        "frame": 3,
        "location": 0.02132163
    },
    {
        "frame": 4,
        "location": 0.02132163
    },
    {
        "frame": 5,
        "location": 0.02132163
    },
    {
        "frame": 6,
        "location": 0.02132163
    },
    {
        "frame": 7,
        "location": 0.02132163
    },
    {
        "frame": 8,
        "location": 0.04264326
    },
    {
        "frame": 9,
        "location": 0.04264326
    }
]
```
