package main

import (
	"encoding/csv"
	"errors"
	"log"
	"math"
	"os"
	"path/filepath"
	"strconv"
	"strings"
)

//This file reads a csv file into an Array of string arrays.
//If there was no error, the returned error will be nil.
//leng specifies how many columns the csv file has (default 2).
//It will be assumed that ; is used to separate the columns and a linebreak indicates the next row.
func readCSVFile(file string, args ...interface{}) (record [][]string, err error) {

	size := 2
	comma := ' '

	for _, arg := range args {
		switch t := arg.(type) {
		case rune:
			comma = t
		case string:
			tmp := []rune(t)
			comma = tmp[0]
		case int:
			size = t
		default:
			return nil, errors.New("invalid argument")
		}
	}

	file = filepath.Clean(file)
	files, err := os.Open(file) // For read access.
	if err != nil {
		log.Fatal(err)
	}
	data := make([]byte, 1000000000)
	count, err := files.Read(data)
	if err != nil {
		log.Fatal(err)
	}

	in := string(data[0:count])

	r := csv.NewReader(strings.NewReader(in))
	r.FieldsPerRecord = size
	r.TrimLeadingSpace = true
	r.Comma = comma

	return r.ReadAll()
}

//This helper function simply parses a string into an float and handles all possible format problems.
func parseFloat(str string) (float64, error) {
	val, err := strconv.ParseFloat(str, 64)
	if err == nil {
		return val, nil
	}
	//Some number may be seperated by comma, for example, 23,120,123, so remove the comma firstly
	str = strings.Replace(str, ",", "", -1)

	//Some number is specifed in scientific notation
	pos := strings.IndexAny(str, "eE")
	if pos < 0 {
		return strconv.ParseFloat(str, 64)
	}

	var baseVal float64
	var expVal int64

	baseStr := str[0:pos]
	baseVal, err = strconv.ParseFloat(baseStr, 64)
	if err != nil {
		return 0, err
	}

	expStr := str[(pos + 1):]
	expVal, err = strconv.ParseInt(expStr, 10, 64)
	if err != nil {
		return 0, err
	}

	return baseVal * math.Pow10(int(expVal)), nil
}
